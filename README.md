# Marketing Design

This is the repository for all assets and issue tracking relating to GNOME marketing and design. It is managed by the engagement team, but serves many teams e.g. GUADEC, LAS, GNOME.Asia, etc.

In this repository you will find an archive and working copies of all art, design, and print assets and materials for the GNOME project and its affiliations.


## Note
This repository uses LFS. You must have Git-LFS installed to be able to properly work with the files here.


Please reach out to either @bwyazel or @chenriksen if you have an questions or interest in helping out, and please let us know if there are any issues with LFS or the files stored in this repository.
