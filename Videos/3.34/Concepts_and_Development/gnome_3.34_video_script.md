#GNOME 3.34 Release Video
##Creative Brief
Create a video which highlights the most interesting new changes and newsworthy additions to GNOME desktop and related GNOME software apps. Video should be fun and tongue-in-cheek playful response to various criticisms levelled at Gnome. Should be done in a way which both acknowledges the user complaints and addresses them using the recent updates. This will show off the fact that GNOME developers do actually listen to users, and has been working hard to make GNOME a great experience both for old users and new.

## Draft outline

### Intro: [Brief display of gnome history showing gnome desktop's transformations (less cheeky version + Britt Edit)]

- With a lot of hard work and dedication, GNOME developers have been refining the GNOME desktop experience with a modern, streamlined approach. [shows an info-graphic styling of GNOME desktop interface changing from one version to the next]

- With the 3.34 release, GNOME makes your day to day tasks, smoother, easier and more enjoyable... [info-graphic style of user sitting cross-legged with a laptop in a serene meditative pose with a halo of data (1001010101) flowing around]

- ... whether you are a developer, content creator, or just an everyday computer user. [data changes to icons of GNOME tools, representing each type of user as they are mentioned, and users change to male/female/different skin colors, etc. to show a variety, Joke: maybe last user is actually just a cat walking over the keyboard]

### Feature 1: Speed increase

To enhance GNOME's modern animation-centric experience, we've been able to drastically improve the responsiveness and consistency of the desktop, so you can enjoy a buttery-smooth GNOME experience on a wide array of hardware devices and generations. [Shows smooth user interface animations on the right, as icons on the right scroll by representing different computers]

[Desktop Tower… Laptop … Netbook ...Tablet … Raspberry Pi  … Potato (vegetable) with a keyboard plugged into it (possibly with a fly buzzing around it)- reference to "potato" - meaning "really bad hardware in gamer-speak"]

### Feature 2: Changes to GNOME shell interface

- Many small changes have been made to the overall look and feel of GNOME with a new shell theme. [fades between old and new theme & supplied content: Settings - Wi-Fi Panel.mp4]

- It's now possible to rearrange and group apps into folders with simple drag and drop. [video from faeneron showing this]

- Managing your important dates has never been easier with a complete calendar manager, which integrates seamlessly with third party calendars. [Supplied content: Calendar management Dialog.mp4]

- Pictures you add to your backgrounds now show up prominently in the panel, and you can change both the desktop and the lock screen backgrounds separately, all in one easy to use interface. [Supplied content: Settings - Background Panel.mp4]

- Search options can now be rearranged and your results will be sorted in the order you choose. [Supplied content: Search Panel.mp4]

- “Settings” now makes use of responsive design, making GNOME easier to use and configure, even on small-screen devices. [Supplied content: Settings - Adaptive Layout.mp4]

### Feature 3: Features for Developers

- It's now easier than ever to code and develop in GNOME, with a wealth of new additions to the system profiler, GNOME Builder, as well as virtualization improvements with Boxes. [clips of each of these things]


### Outro:

- But really, this is all just the tip of the iceberg. [animation of figure planting a flag on the top of an icy mountain (zoomed in), then zoom out to show the "iceberg" is the GNOME logo] 

- Come join our active, friendly community to find out more about what GNOME can do for your everyday work and play. [GNOME logo turns to a clipping mask with illustrations of gnome community members inside waving at the camera]. [GNOME logo fades out to the white version on blue in the intro with website URL below, outro music] [fades to white screen with gnome video information, music fades] [fades to black, no audio]
