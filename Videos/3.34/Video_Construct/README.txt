Hello! C.Rogers, here. If you're reading this, you're the unfortunate soul who has inherited these files... possibly to build the next video, or maybe you just want to see how it was done for your own FLOSS animation purposes.

While I've done my best to clean up and simplify the original file structure, no doubt I probably missed something, somewhere. If you happen to find an error, do help the next person by submitting a patch to fix what's wrong.

The first thing you'll need is Blender 2.81 or greater. This file relies on the new EEVEE rendering engine, which takes a small fraction of the render time of Cycles, not to mention previews in near real-time on somewhat less than powerful hardware.

I've used the "import images as planes" extension to pull in the flat graphics i've exported to the "graphics" folder, and you'll find the svg source file with all the bits in the same folder as this readme (gnome_3.34_release_video_graphics_construct.svg).

Where possible, I've converted the frame output to video files, and replaced them with rendered videos (all added to the "clips" folder) so you should only have to render frames of these items:

Scene: main_animation render to /frames/main_animation_frames
Scene: user_cat rendder to /frames/cat_subanim_frames

Other scenes in the animation file are:

Scene: green_chroma_key - I set up to add the wallpaper to screen captures which included green backgrounds. I highly recommend not doing this, as the quality isn't as good as a screen capture which already includes the background.

Scene: final_composite - This is the final composite of all the clips, music, audio, etc. This is what renders the final video.


Good luck!


